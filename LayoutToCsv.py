import pandas as pd
import re
import csv
import os

linhaArquivo = ""
listDados = []
listSocios = []
listCnae = []
layoutDados = ['CNPJ', 'Matriz', 'RazaoSocial', 'NomeFantasia', 'SituacaoCadastrao','DataSituacao','MotivoSituacao','CodigoJuridico','DataInicio','CNAE','Endereco','EmailContato','CapitalSocial','PorteEmpresa','OpcaoSimples','OpcaoMei','SituacaoEspecial','DataSituacao']
layoutSocios = ['CNPJ','Identificador','Nome','CPF/CNPJ','PercentualCapital','DataEntrada','CodigoPais','NomePais','CPFRepresentante','NomeRepresentante']
layoutCNAE = ['CNPJ','CNAE']

def getCNPJ():
    return linhaArquivo[3:17]
def getMatriz():
    return linhaArquivo[17:18]
def getRazaoSocial():
    return linhaArquivo[18:168].replace("  ","")
def getNomeFantasia():
    return linhaArquivo[168:223].replace("  ","")
def getSituacaoCadastral():
    return linhaArquivo[223:225]
def getDateSituacao():
    return linhaArquivo[225:233]
def getMotivoSituacao():
    return linhaArquivo[233:235]
def getCodigoJuridico():
    return linhaArquivo[363:367]
def getDataInicio():
    return linhaArquivo[367:375]
def getCNAE():
    return linhaArquivo[375:382]
#todo
def trataEndereco(endreco):
    return endreco

def getEndereco(layout):
    if layout == 'dados':
        return trataEndereco(linhaArquivo[382:688]).replace("  ","")
    return ""
def getEmail():
    return linhaArquivo[774:899].replace("  ","")
def getPorteEmpresa():
    porte = linhaArquivo[905:907]
    if porte == '00':
        return "NAO INFORMADO"
    else:
        if porte == '01':
            return "MICRO EMPRESA"
        else:
            if porte == '03':
                return "EMPRESA DE PEQUENO PORTE"
            else:
                if porte == '05':
                    return "DEMAIS"
                else:
                    return "ERRO PORTE "+porte
    return porte
def getOpcaoSimples():
    porte = linhaArquivo[907:908]
    if porte == '0':
        return "NAO OPTANTE"
    else:
        if porte == '5' or porte == '7':
            return "OPTANTE PELO SIMPLES"
        else:
            if porte == '6' or porte == '8':
                return "EXCLUIDO DO SIMPLES"
            else:
                return "ERRO OPCAO SIMPLES "+porte
    return porte
def getOpcaoMei():
    linha = linhaArquivo[924:925]
    if linha == 'S':
        return "SIM"
    else: 
        return "NAO"
def getSituacaoEspecial():
    return linhaArquivo[925:948].replace("  ","")
def getCapitalSocial():
    return linhaArquivo[891:905].replace("  ","")
def getIdentificadorSocio():
    identificador = linhaArquivo[17:18]
    if identificador == '1':
        return "PESSOA JURIDICA"
    else: 
        if identificador == '2':
            return "PESSOA FISICA"
        else:
            if identificador == '3':
             return "ESTRANGEIRO"
    return "IDENTIFICADOR NAO RECONHECIDO -> "+identificador
def getNomeSocio():
    return linhaArquivo[18:168].replace("  ","")
def getSocioDoc():
    return linhaArquivo[169:182]
def getSocioPercentual():
    return linhaArquivo[184:189]
def getSocioDataEntrada():
    return linhaArquivo[189:197]
def getSocioCodigoPais():
    return linhaArquivo[197:200]    
def getSocioNomePais():
    return linhaArquivo[200:270].replace("  ","")
def getCpfRep():
    return linhaArquivo[271:281]
def getNomeRep():
    return linhaArquivo[281:341]
def getCNAESecundaria():
    listCnaes =  re.findall('.......', linhaArquivo[17:711].replace("0000000",""))
    return ';'.join(str(e) for e in listCnaes)
def getTipoAtualizacao():
    identificador = linhaArquivo[2:3]
    if identificador == 'A':
        return "ATUALIZACAO DO ESTABELECIMENTO"
    else: 
        if identificador == 'I':
            return "INCLUSAO DE UM NOVO ESTABELECIMENTO"
        else:
            if identificador == 'E':
             return "EXCLUSAO DO ESTABELECIMENTO"
    return "IDENTIFICADOR NAO RECONHECIDO -> "+identificador
def buildLayoutDados():
    return getCNPJ()+";"+getMatriz()+";"+getRazaoSocial()+";"+getNomeFantasia()+";"+getSituacaoCadastral()+";"+getDateSituacao()+";"+getMotivoSituacao()+";"+getCodigoJuridico()+";"+getDataInicio()+";"+getCNAE()+";"+getEndereco('dados')+";"+getEmail()+";"+getCapitalSocial()+";"+getPorteEmpresa()+";"+getOpcaoSimples()+";"+getOpcaoMei()+";"+getSituacaoEspecial()+";"+getDateSituacao()
def buildLayoutSocios():
    return getCNPJ()+";"+getIdentificadorSocio()+";"+getNomeSocio()+";"+getSocioDoc()+";"+getSocioPercentual()+";"+getSocioDataEntrada()+";"+getSocioCodigoPais()+";"+getSocioNomePais()+";"+getCpfRep()+";"+getNomeRep()
def buildCnae():
    return getCNPJ()+";"+getCNAESecundaria()+";"+getTipoAtualizacao()

def createCsv():
    if not os.path.exists('./dados.csv'):
        with open('dados.csv', 'wb') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerow(layoutDados)
    if not os.path.exists('./socios.csv'):
        with open('socios.csv', 'wb') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerow(layoutSocios)
    if not os.path.exists('./cnae.csv'):
        with open('cnae.csv', 'wb') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerow(layoutCNAE)


def writeCSV(path,lista):
    createCsv()
    with open(path, mode='a') as csv_file:
        for dado in lista:
            st = ";".join(str(x) for x in dado)
            csv_file.write(st+"\n")
            
#Layout Dados Cadastrais
dfDados = pd.DataFrame(columns = layoutDados)
dfSocios = pd.DataFrame(columns = layoutSocios)
dfCNAE = pd.DataFrame(columns = layoutCNAE)

filepath = 'cnpjs.txt'  
with open(filepath) as fp:
   for cnt, line in enumerate(fp):
        linhaArquivo = line
        if(linhaArquivo[0:2] == '1F'):
           listDados.append(buildLayoutDados().split(';'))
        else: 
            if (linhaArquivo[0:2] == '2F'):
                listSocios.append(buildLayoutSocios().split(';'))
            else: 
                if linhaArquivo[0:2] == '6F':
                    listCnae.append(buildCnae().split(';'))
        if len(listDados)>100000:
            writeCSV("dados.csv",listDados)
        if len(listSocios)>100000:
            writeCSV("socios.csv",listSocios)
        if len(listCnae)>100000:
            writeCSV("cnae.csv",listCnae)
writeCSV("dados.csv",listDados)
writeCSV("socios.csv",listSocios)
writeCSV("cnae.csv",listCnae)


#end of File
